Import-Module ActiveDirectory

$username=Read-Host "enter username"
$user = Get-ADUser $username -Properties proxyAddresses
$user.proxyAddresses.Add("SMTP:"+$username+"@mydomain.tld")
$user.proxyAddresses.Add("smtp:"+$username+"@yourorgname.onmicrosoft.com")
Set-ADUser -Instance $user

