#"constants"
$mdmgroup = "guid here" #guid of group used for MDM

#set up connections
$cred = Get-Credential
Import-Module MsOnline
Connect-MsolService -Credential $cred
Connect-SPOService -Url https://yourorg-admin.sharepoint.com -Credential $cred
$exchangeSession = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri "https://outlook.office365.com/powershell-liveid/" -Credential $cred -Authentication Basic -AllowRedirection
Import-PSSession $exchangeSession

#add to O365 security group for MDM
$username = Read-Host "enter username"
$user = Get-MsolUser -UserPrincipalName ($username+"@yourdomain.tld")
Add-MsolGroupMember -GroupObjectId $mdmgroup -GroupMemberType User -GroupMemberObjectId $user.ObjectId

#add to sharepoint group somegroup
Add-SPOUser -Site https://yourorg.sharepoint.com -LoginName ($username+"@yourdomain.tld") -Group "somegroup"

#set MRM policy to no policy
Set-Mailbox -Identity $username -RetentionPolicy $null

#add to needed distribution groups
Add-DistributionGroupMember -Identity "distribution group name" -Member $username

#gravy
$tType = Read-Host "stuff"
switch -Regex ($tType)
{
    "[tT]" {
                #do stuff
		break
            }
    "[rR]" {
                #do stuff
		break
            }
    default {"I am error"}
}

#disconnect
Get-PSSession | Remove-PSSession
Disconnect-SPOService

