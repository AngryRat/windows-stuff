#remove all "temp" files in each user profile
Set-Location "C:\Users"
Remove-Item ".\*.*\Desktop\*" -Recurse -Force
Remove-Item ".\*.*\Documents\*" -Recurse -Force
Remove-Item ".\*.*\Downloads\*" -Recurse -Force
Remove-Item ".\*.*\Music\*" -Recurse -Force
Remove-Item ".\*.*\OneDrive\*" -Recurse -Force
Remove-Item ".\*.*\Pictures\*" -Recurse -Force
Remove-Item ".\*.*\Videos\*" -Recurse -Force

#repopulate shortcuts
#Copy-Item "C:\shortcuts\*" "C:\Users\Public\Desktop"

